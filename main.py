# Define the strategy interface
class TextFormatter:
    def format(self, text):
        pass

# Implement concrete strategy classes
class LowercaseFormatter(TextFormatter):
    def format(self, text):
        return text.lower()

class UppercaseFormatter(TextFormatter):
    def format(self, text):
        return text.upper()

class CamelCaseFormatter(TextFormatter):
    def format(self, text):
        words = text.split(' ')
        camel_case_text = ''

        for word in words:
            camel_case_text += word.capitalize()

        return camel_case_text[0].lower() + camel_case_text[1:]

# Create a context class that uses the selected text formatting strategy
class TextProcessor:
    def __init__(self, text_formatter):
        self.text_formatter = text_formatter

    def process_text(self, text):
        return self.text_formatter.format(text)

# Client code
text = "Hello, World! This is a Text Formatting Example."

# Choose the text formatting strategy at runtime
chosen_text_formatter = CamelCaseFormatter()  # You can change this to Lowercase or Uppercase

text_processor = TextProcessor(chosen_text_formatter)
formatted_text = text_processor.process_text(text)

print("Formatted text:", formatted_text)
